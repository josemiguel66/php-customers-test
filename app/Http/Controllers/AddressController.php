<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Customer;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($customer)
    {

        $customer = Customer::with('company')->findOrFail($customer);
        $address = Address::whereHas('customer', function ($query) use ($customer) {
            $query->where('id', $customer->id);
        })->get();
        return view('customers.address.list', ['address'=>$address, 'customer'=> $customer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer)
    {
        $customer = Customer::with('company')->findOrFail($customer);

        $data = (object)[
            'route'  => route('customers.addresses.store', $customer->id),
            'method' => 'POST',
            'fields' => (object)[
                'address' => '',
            ],
        ];
        return view('customers.address.form', ['data'=> $data, 'customer'=> $customer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($customer, Request $request)
    {
        $this->validate($request, [
            'address' => 'required',
        ]);

        Customer::findOrFail($customer)->address()->create($request->all());

        return redirect()->route('customers.addresses.index', $customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit($customer, $address)
    {
        $address = Address::with('customer')->where('id', $address)->whereHas('customer', function ($query) use ($customer) {
            $query->where('id', $customer);
        })->firstOrFail();

        $customer = $address->customer;

        $data = (object)[
            'route'  => route('customers.addresses.update', ['customer'=> $customer, 'address'=> $address->id]),
            'method' => 'PUT',
            'customer' => $customer,
            'fields' => $address
        ];
        return view('customers.address.form', ['data'=> $data, 'customer'=> $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update($customer, Request $request, $address)
    {
        $this->validate($request, [
            'address'    => 'required',
        ]);

        $address = Address::where('id', $address)->whereHas('customer', function ($query) use ($customer) {
            $query->where('id', $customer);
        })->firstOrFail();
        $address->update($request->all());
        return redirect()->route('customers.addresses.index', $customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer, $address)
    {
        $address = Address::where('id', $address)->whereHas('customer', function ($query) use ($customer) {
            $query->where('id', $customer);
        })->firstOrFail();
        $address->delete();
        return redirect()->route('customers.addresses.index', $customer);
    }
}
