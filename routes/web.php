<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/customers/');
});

Route::resource('customers', CustomerController::class);

Route::prefix('customers')->name('customers.')->group(function () {
     /**
     * por ser modo test se realizó de esta manera
     */
    Route::get('/{id}/destroy',  [CustomerController::class, 'destroy'])->name('delete');

    Route::resource('/{customer}/addresses', AddressController::class);

    /**
     * por ser modo test se realizó de esta manera
     */
    Route::get('/{customer}/addresses/{address}/destroy',  [AddressController::class, 'destroy'])->name('addresses.delete');
});
