@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12 text-center p-4">
        <a class="btn btn-primary" href="{{ route('customers.create') }}" role="button">Agregar</a>
    </div>
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Compañía</th>
                            <th scope="col">Cliente</th>
                            <th scope="col">Direcciones</th>
                            <th scope="col">Acciones</th>
                        </tr>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customers as $item)
                        <tr>
                            <th scope="row">{{ $item->id }}</th>
                            <td>{{ $item->company->name }}</td>
                            <td>{{ $item->name }}</td>
                            <td>
                                <a class="link" href="{{ route('customers.addresses.index', ['customer'=> $item->id]) }}">Direcciones</a>
                            </td>
                            <td>
                                <a class="link" href="{{ route('customers.edit', $item->id) }}">Editar</a>
                                <a class="link eliminar" href="{{ route('customers.delete', $item->id) }}">Eliminar</a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts-footer')
<script>

</script>

@endsection
