@extends('layouts.app')
@section('content')
<form id="form1" action="{{ $data->route }}" method="POST" enctype="multipart/form-data">

<div class="row">
    <div class="col-md-12 text-center p-4">
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a name="" id="" class="btn btn-primary" href="{{ route('customers.index') }}" role="button">Cancelar</a>
    </div>
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body">
                    @csrf
                    @method($data->method)

                    <div class="mb-3">
                        <label for="name" class="form-label">Compañía</label>
                          <label for=""></label>
                          <select class="form-control" name="company_id" id="company_id">
                            <option value="">Seleccione</option>
                            @foreach ($data->companies as $company)
                                <option value="{{ $company->id }}" {{ ((old('company_id') ? old('company_id') : $data->fields->company_id) == $company->id) ? 'selected' : '' }}>{{ $company->name }}</option>
                            @endforeach
                          </select>
                    </div>
                    <div class="mb-3">
                        <label for="name" class="form-label">Nombre</label>
                        <input type="text" class="form-control" id="name" name="name" value="@if (old('name')){{ old('name') }}@else{{ $data->fields->name }}@endif" required>
                    </div>


            </div>
        </div>
    </div>
</div>
</form>

@endsection
@section('scripts-footer')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function () {

        $('#company_id').select2();
    });
</script>

@endsection
