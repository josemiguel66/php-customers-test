@extends('layouts.app')
@section('content')
<div class="row">

    <div class="col-md-8 offset-md-2 text-center p-4">
        <a class="btn btn-primary" href="{{ route('customers.addresses.create', ['customer'=> $customer]) }}" role="button">Agregar</a>
        <a class="btn btn-primary" href="{{ route('customers.index') }}" role="button">Listado de clientes</a>

    </div>
    <div class="col-md-8 offset-md-2">
        <strong>Empresa: </strong>{{ $customer->company->name }}<br>
        <strong>Cliente: </strong>{{ $customer->name }}
    </div>


    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">#</th>
                            <th scope="col" width="80%">Dirección</th>
                            <th scope="col" width="10%">Acciones</th>
                        </tr>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($address as $item)
                        <tr>
                            <th scope="row">{{ $item->id }}</th>
                            <td>{{ $item->address }}</td>
                            <td>
                                <a class="link" href="{{ route('customers.addresses.edit', ['customer'=> $customer->id, 'address'=> $item->id]) }}">Editar</a>
                                <a class="link" href="{{ route('customers.addresses.delete', ['customer'=> $customer->id, 'address'=> $item->id]) }}">Eliminar</a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts-footer')
<script>

</script>

@endsection
