@extends('layouts.app')
@section('content')
<form id="form1" action="{{ $data->route }}" method="POST" enctype="multipart/form-data">

<div class="row">
    <div class="col-md-8 offset-md-2 text-center p-4">
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a name="" id="" class="btn btn-primary" href="{{ route('customers.addresses.index', $customer->id) }}" role="button">Cancelar</a>
    </div>
    <div class="col-md-8 offset-md-2">
        <strong>Empresa: </strong>{{ $customer->company->name }}<br>
        <strong>Cliente: </strong>{{ $customer->name }}
    </div>
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body">
                    @csrf
                    @method($data->method)
                    <div class="mb-3">
                        <label for="address" class="form-label">Dirección</label>
                          <textarea class="form-control" name="address" id="address" rows="3" required>@if (old('address')){{ old('address') }}@else{{ $data->fields->address}}@endif</textarea>
                    </div>


            </div>
        </div>
    </div>
</div>
</form>

@endsection
@section('scripts-footer')
<script>

</script>

@endsection
